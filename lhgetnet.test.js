const lhgetnet = require('./lhgetnet');
const axios = require("axios");

const api = new lhgetnet({
    client_id: 'd5e293d3-d56d-411f-8fdd-6699b35cb3e8',
    client_secret: '24d3f387-b586-4b7d-b2b2-e14ff9805021',
    seller_id: 'a9bb50b4-47e5-43d4-af1a-263c8b3148a2',
    debug: true
});

let card_id = null;
let number_token = null;
let expiration_month = null;
let expiration_year = null;
let payment_id = null;
let payer_authentication_response = null;

test('LHGetNet.getOAuthToken', () => {
    return api.getOAuthToken().then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.access_token).toBeTruthy();
    });
}, 30000);

test('LHGetNet.tokenizarCartao', () => {
    return api.tokenizarCartao({
        card_number: "5155901222280001", 
        customer_id: "12345",
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.number_token).toBeTruthy();
        number_token = resp.number_token;
    });
}, 30000);

test('LHGetNet.armazenarCartaoCofre', () => {
    return api.armazenarCartaoCofre({ 
        number_token,
        cardholder_name: "JOAO DA SILVA", 
        expiration_month: "12", 
        expiration_year: "21", 
        customer_id: "12345",
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.card_id).toBeTruthy();
        expect(resp.number_token).toBeTruthy();
        card_id = resp.card_id;
    });
}, 30000);

test('LHGetNet.listarCartoesCofre', () => {
    return api.listarCartoesCofre({ 
        customer_id: "12345"
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.cards).toBeTruthy();
        expect(resp.cards.length).toBeGreaterThan(0);
    });
}, 30000);

test('LHGetNet.obterCartaoCofre', () => {
    return api.obterCartaoCofre({ 
        card_id
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.card_id).toBeTruthy();
        expect(resp.number_token).toBeTruthy();
        number_token = resp.number_token;
        expiration_month = resp.expiration_month;
        expiration_year = resp.expiration_year;
    });
}, 30000);

test('LHGetNet.verificarCartao', () => {
    return api.verificarCartao({ 
        number_token, expiration_month, expiration_year,
        cardholder_name: "JOAO DA SILVA",
        security_code: "123"
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.status).toBeTruthy();
        expect(resp.verification_id).toBeTruthy();
        expect(resp.authorization_code).toBeTruthy();
        expect(resp.status).toBe("VERIFIED");
    });
}, 30000);

test('LHGetNet.removerCartaoCofre', () => {
    return api.removerCartaoCofre({ 
        card_id
    }).then( resp => {
        expect(resp).toBeFalsy()
    });
}, 30000);

test('LHGetNet.pagarCartaoCredito', () => {
    return api.pagarCartaoCredito({ 
        amount: 100,
        currency: "BRL",
        order: {
            order_id: "6d2e4380-d8a3-4ccb-9138-c289182818a3",
            sales_tax: 0,
            product_type: "service"
        },
        customer: {
            customer_id: "12345",
            name: "João da Silva",
            email: "customer@email.com.br",
            document_type: "CPF",
            document_number: "12345678912",
            phone_number: "5551999887766",
            billing_address: {
                street: "Av. Brasil",
                number: "1000",
                complement: "Sala 1",
                district: "São Geraldo",
                city: "Porto Alegre",
                state: "RS",
                country: "Brasil",
                postal_code: "90230060"
            }
        },
        device: {
            ip_address: "127.0.0.1",
            device_id: "hash-device-id"
        },
        shippings: [{
            first_name: "João",
            name: "João da Silva",
            email: "customer@email.com.br",
            phone_number: "5551999887766",
            shipping_amount: 3000,
            address: {
                street: "Av. Brasil",
                number: "1000",
                complement: "Sala 1",
                district: "São Geraldo",
                city: "Porto Alegre",
                state: "RS",
                country: "Brasil",
                postal_code: "90230060"
            }
        }],
        credit: {
            delayed: true,
            authenticated: false,
            pre_authorization: false,
            save_card_data: false,
            transaction_type: "FULL",
            number_installments: 1,
            soft_descriptor: "LOJA*TESTE*COMPRA-123",
            dynamic_mcc: 1799,
            card: {
                number_token,
                cardholder_name: "JOAO DA SILVA",
                security_code: "123",
                expiration_month,
                expiration_year
            }
        }
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.payment_id).toBeTruthy();
        expect(resp.status).toBe("AUTHORIZED"); // Autrorizado por causa do delayed true
        payment_id = resp.payment_id;
    });
}, 30000);

test('LHGetNet.confirmarPagamentoCartaoCredito', () => {
    return api.confirmarPagamentoCartaoCredito({ 
        amount: 100,
        payment_id
    }).then(resp => {
        expect(resp).toBeTruthy();
        expect(resp.payment_id).toBeTruthy();
        expect(resp.status).toBe("CONFIRMED");
    });
}, 30000);

test('LHGetNet.cancelarPagamentoCartaoCredito', () => {
    return api.cancelarPagamentoCartaoCredito({
        payment_id
    }).then(resp => {
        expect(resp).toBeTruthy();
        expect(resp.payment_id).toBeTruthy();
        expect(resp.status).toBe("CANCELED");
    });
}, 30000);

test('LHGetNet.tokenizarCartao', () => {
    return api.tokenizarCartao({
        card_number: "5155901222280001", 
        customer_id: "12345"
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.number_token).toBeTruthy();
        number_token = resp.number_token;
    });
}, 30000);

test('LHGetNet.pagarCartaoDebito', () => {
    return api.pagarCartaoDebito({ 
        amount: 100,
        currency: "BRL",
        order: {
            order_id: "6d2e4380-d8a3-4ccb-9138-c289182818a3",
            sales_tax: 0,
            product_type: "service"
        },
        customer: {
            customer_id: "12345",
            first_name: "João",
            last_name: "da Silva",
            name: "João da Silva",
            email: "customer@email.com.br",
            document_type: "CPF",
            document_number: "12345678912",
            phone_number: "5551999887766",
            billing_address: {
                street: "Av. Brasil",
                number: "1000",
                complement: "Sala 1",
                district: "São Geraldo",
                city: "Porto Alegre",
                state: "RS",
                country: "Brasil",
                postal_code: "90230060"
            }
        },
        device: {
            ip_address: "127.0.0.1",
            device_id: "hash-device-id"
        },
        shippings: [{
            first_name: "João",
            name: "João da Silva",
            email: "customer@email.com.br",
            phone_number: "5551999887766",
            shipping_amount: 3000,
            address: {
                street: "Av. Brasil",
                number: "1000",
                complement: "Sala 1",
                district: "São Geraldo",
                city: "Porto Alegre",
                state: "RS",
                country: "Brasil",
                postal_code: "90230060"
            }
        }],
        debit: {
            cardholder_mobile: "5551999887766",
            soft_descriptor: "LOJA*TESTE*COMPRA-123",
            card: {
                number_token,
                cardholder_name: "JOAO DA SILVA",
                security_code: "123",
                expiration_month,
                expiration_year
            }
        }
    }).then(resp => {
        expect(resp).toBeTruthy();
        expect(resp.payment_id).toBeTruthy();
        expect(resp.redirect_url).toBeTruthy();
        expect(resp.post_data).toBeTruthy();
        expect(resp.post_data.issuer_payment_id).toBeTruthy();
        expect(resp.post_data.payer_authentication_request).toBeTruthy();
        payment_id = resp.payment_id;

        return axios.post(resp.redirect_url, {
            MD: resp.post_data.issuer_payment_id,
            PaReq: resp.post_data.payer_authentication_request,
            TermUrl: 'https://minha_url_teste'
        }).then( axiosResp => {
            expect(axiosResp).toBeTruthy();
            expect(axiosResp.data).toBeTruthy();
            
            // Obtendo o dado do formulário de Testes do GetNet, em Abiente de produção
            // deve-se exibir o formulário do banco para o cliente poder autenticar
            var doc = new DOMParser().parseFromString(axiosResp.data, "text/html");
            expect(doc.getElementsByName('PaRes')[0].value).toBeTruthy();
            payer_authentication_response = doc.getElementsByName('PaRes')[0].value;
        });
    });
}, 30000);

test('LHGetNet.finalizacaoPagamentoAutenticadoCartaoDebito', () => {
    return api.finalizacaoPagamentoAutenticadoCartaoDebito({
        payment_id, payer_authentication_response
    }).then( resp => {
        expect(resp).toBeTruthy();
        expect(resp.status).toBe("APPROVED");
    });
}, 30000);

// Nao conseguir gerar uma transação pre autorizada para testar.
// Vai ficar no TODO
// test('LHGetNet.ajustarValorTransacaoCreditoPreAutorizada', () => {
//     return api.ajustarValorTransacaoCreditoPreAutorizada({ 
//         amount: 120,
//         payment_id
//     }).then(resp => {
//         console.log("RESP CONFIRMACAO DE PAGAMENTO:", resp);
//     });
// });