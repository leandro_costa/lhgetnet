# LHGETNET #

Implementação da API do GETNET

### What is this repository for? ###

* Implementação em Node.js de métodos para consumo da API do GETET (https://www.getnet.com.br/)
* Version 1.0.0

### Instalação ###

* npm install --save lhgetnet

### Utilização ###

* const lhgetnet = require('lhgetnet');

* const api_getnet = new lhgetnet({
*     client_id,
*     client_secret,
*     seller_id,
*     debug: process.env.NODE_ENV === 'development'
* });

### Com quem estou Falando ? ###

* Leandro Costa - contato@leandrocosta.pro.br